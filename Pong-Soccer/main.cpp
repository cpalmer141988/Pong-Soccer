//Libraries
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <map>
#include <conio.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <ctime>

//Global variables, functions, classes

//C++ Program Entry Point
int main(){
	
	//Window Creation
	sf::Vector2i screenDimensions(800, 600);
	sf::Vector2i blockDimensions(10,10);

	sf::RenderWindow window(sf::VideoMode(screenDimensions.x,screenDimensions.y), "SFML Game", sf::Style::Default);
	//sf::Vector2u size(400,400);
	//window.setSize(size);

	//Clock
	sf::Clock clock;

	//Time
	//sf::Time time;
	//sf::Time time = sf::milliseconds(0);
	//std::cout << time.asMilliseconds() << std::endl;

	//Setting the framerate limit to 60 FPS
	window.setFramerateLimit(60);

	//Disables Key Press Repetition
	//window.setKeyRepeatEnabled(false);

	//Variable that keeps the game loop running
	bool play = true;

	//Create Event object before Game Loop so we don't recreate it each frame
	sf::Event event;

	//States
	bool w = false;
	bool a = false;
	bool s = false;
	bool d = false;

	bool initiateGame = true;

	bool update = false;

	//Variables
	int playerSpeed = 4;
	int aiSpeed = 1;
	int ballSpeed = 3;
	int scoreToWin = 5;

	int yVelocityPad1 = 0;
	int yVelocityPad2 = 0; //AI
	int xVelocityBall = 0;//-3;
	int yVelocityBall = 0;//-3;
	int pad1Score = 0;
	int pad2Score = 0;
	std::string display = "";

	//Images

	//Textures
	sf::Texture tex_pad;
	sf::Texture tex_ball;
	sf::Texture tex_bg;
	sf::Texture tex_tileBg;

	if (tex_pad.loadFromFile("Images/pad.png") == false){
		return -1;
	}
	if (tex_ball.loadFromFile("Images/ball.png") == false){
		return -1;
	}
	if (tex_bg.loadFromFile("Images/background.png") == false){
		return -1;
	}
	if (tex_tileBg.loadFromFile("Images/real_tiles.png") == false){
		return -1;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  Sprites
	enum Direction { Down, Left, Right, Up };

	//Player 1
	sf::Texture tex_zatoichi;
	if (tex_zatoichi.loadFromFile("Images/zatoichi.png") == false){
		return -1;
	}
	sf::Sprite zatoichi;
	zatoichi.setOrigin(16,16);
	zatoichi.setPosition(300,325);
	zatoichi.setTexture(tex_zatoichi);
	sf::Vector2i p1_source(1, Down);
	zatoichi.setTextureRect(sf::IntRect(p1_source.x * 32, p1_source.y * 32, 32, 32));

	//Player 2
	sf::Texture tex_hrlyqunn;
	if (tex_hrlyqunn.loadFromFile("Images/hrly_qunn.png") == false){
		return -1;
	}
	sf::Sprite hrlyqunn;
	hrlyqunn.setOrigin(16,16);
	hrlyqunn.setPosition(500,325);
	hrlyqunn.setTexture(tex_hrlyqunn);
	sf::Vector2i p2_source(1, Down);
	zatoichi.setTextureRect(sf::IntRect(p2_source.x * 32, p2_source.y * 32, 32, 32));

	float frameCounter = 0, switchFrame = 100, frameSpeed = 600;
	bool p1_walkCycle = false;
	int p1Frame = 0;
	bool p2_walkCycle = false;
	int p2Frame = 0;
	std::array<int,4> walkFrames = {1, 2, 1, 0};
				
				

	sf::View p1_view, p2_view, shared_view;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////// Single Screen View Setup
	p1_view.reset(sf::FloatRect(0, 0, screenDimensions.x, screenDimensions.y));
	p1_view.setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));

	sf::Vector2f position(screenDimensions.x / 2, screenDimensions.y / 2);

	sf::RectangleShape divider;
	divider.setSize(sf::Vector2f(10, 550));
	divider.setOrigin(5,0);
	divider.setPosition(400,50);
	divider.setFillColor(sf::Color::Green);
		
	/////////////////Shapes
	//Background
	sf::RectangleShape bg;
	bg.setSize(sf::Vector2f(screenDimensions.x, screenDimensions.y - 50));
	bg.setPosition(0,50);
	//bg.setTexture(&tex_bg);
	bg.setTexture(&tex_tileBg);

	//Pad1
	sf::RectangleShape pad1;
	pad1.setSize(sf::Vector2f(20,100));
	pad1.setOrigin(10,50);
	pad1.setPosition(20,325); //Position
	pad1.setFillColor(sf::Color::Black);
	pad1.setOutlineThickness(5);
	pad1.setOutlineColor(sf::Color::Blue);
	//pad1.setTexture(&tex_pad);

	//Pad2
	sf::RectangleShape pad2;
	pad2.setSize(sf::Vector2f(20,100));
	pad2.setOrigin(10,50);
	pad2.setPosition(780,325); //Position
	pad2.setFillColor(sf::Color::Black);
	pad2.setOutlineThickness(5);
	pad2.setOutlineColor(sf::Color::Red);
	//pad2.setTexture(&tex_pad);

	//Ball
	sf::RectangleShape ball;
	ball.setSize(sf::Vector2f(50,50));
	ball.setOrigin(25,25); //set origin relative to the object based on it's size and 0,0 position
	ball.setPosition(400,325);
	ball.setTexture(&tex_ball);

	sf::Vector2f ball_lastPos;
	sf::Vector2f ball_nextPos;

	//Fonts
	sf::Font arial_r;
	if (arial_r.loadFromFile("Fonts/arial.ttf") == 0){
		return -1;
	}

	sf::String sentence;

	//Text Objects
	sf::Text score;
	score.setFont(arial_r);
	score.setCharacterSize(30);
	score.setColor(sf::Color::Red);
	score.setPosition(371,10);
	score.setString("0 : 0");

	sf::Text winner;
	winner.setFont(arial_r);
	winner.setCharacterSize(60);
	winner.setColor(sf::Color::Red);
	winner.setPosition(225,225);
	//Game Over
	bool gameOver = false;

	//Sound FX
	sf::SoundBuffer buff_hit;
	if (buff_hit.loadFromFile("SoundFX/hit.wav") == false){
		return -1;
	}
	sf::Sound hit;
	hit.setBuffer(buff_hit);

	//Music

	//Game Loop
	while (play == true){
		//Time Test
		//time = clock.getElapsedTime();
		//std::cout << time.asMilliseconds() << std::endl;
		//clock.restart(); //also returns the timestamp before resetting the clock

		//EVENTS
		while (window.pollEvent(event)){
			switch(event.type){
				case sf::Event::KeyPressed:
					if (event.key.code == sf::Keyboard::Escape){
						play = false;
					}
					break;
				case sf::Event::KeyReleased:
					break;
				case sf::Event::MouseEntered:
					std::cout << "Mouse within screen bounds" << std::endl;
					break;
				case sf::Event::MouseLeft:
					std::cout << "Mouse NOT within screen bounds" << std::endl;
					break;
				case sf::Event::MouseMoved:
					//std::cout << "X: " << event.mouseMove.x << " Y: " << event.mouseMove.y << std::endl;
					break;
				case sf::Event::MouseButtonPressed:
					switch(event.mouseButton.button){
						case sf::Mouse::Left:
							std::cout << "Left-Click at X: " << event.mouseButton.x << " Y: " << event.mouseButton.y << std::endl;
							break;
						case sf::Mouse::Right:
							std::cout << "Right-Click at X: " << event.mouseButton.x << " Y: " << event.mouseButton.y << std::endl;
							break;
					}
					break;
				case sf::Event::MouseWheelMoved:
					std::cout << "Delta: " << event.mouseWheel.delta << std::endl; //Down == - || Up == +
					break;
				case sf::Event::GainedFocus:
					update = true;
					break;
				case sf::Event::LostFocus:
					update = false;
					break;
				case sf::Event::Resized:
					std::cout << "Width : " << event.size.width << " Height : " << event.size.height << std::endl;
					break;
				case sf::Event::Closed:
					//Window Close Events
					if (event.type == sf::Event::Closed){
						play = false;
					}
					break;
			}
		}

		if (gameOver){
			/////////////////////////////////////////////////////////////////////////// Game Over
			//Wait for them to close the game, or program a reset option
		} else {
			/////////////////////////////////////////////////////////////////////////// Play Game
			
			//////////////////////Initiate Game (once per game)
			if (initiateGame){
				zatoichi.move(1,0);
				hrlyqunn.move(-1,0);
				initiateGame = false;
			}

			//////////////////////////////////////////////////////////////////////////////////////////////////////// AI
			//Pad1 AI (before needed known movement so it's not psychic)
			if (xVelocityBall < 0){
				if (ball.getPosition().y < pad1.getPosition().y){
					yVelocityPad1 = -aiSpeed;
				} else if (ball.getPosition().y > pad1.getPosition().y){
					yVelocityPad1 = aiSpeed;
				} else {
					yVelocityPad1 = 0;
				}
			} else {
				yVelocityPad1 = 0;
			}

			//Pad2 AI (before needed known movement so it's not psychic)
			if (xVelocityBall > 0){
				if (ball.getPosition().y < pad2.getPosition().y){
					yVelocityPad2 = -aiSpeed;
				} else if (ball.getPosition().y > pad2.getPosition().y){
					yVelocityPad2 = aiSpeed;
				} else {
					yVelocityPad2 = 0;
				}
			} else {
				yVelocityPad2 = 0;
			}

			pad1.move(0, yVelocityPad1);
			pad2.move(0, yVelocityPad2);

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////  LIVE EVENTS
			//Player 1
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
				p1_source.y = Up;
				zatoichi.move(0,-playerSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
				p1_source.y = Left;
				zatoichi.move(-playerSpeed,0);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
				p1_source.y = Down;
				zatoichi.move(0,playerSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
				p1_source.y = Right;
				zatoichi.move(playerSpeed,0);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::A)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::D) ){
				p1_walkCycle = true;
			} else {
				p1_walkCycle = false;
			}

			//Player 2
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				p2_source.y = Up;
				hrlyqunn.move(0,-playerSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				p2_source.y = Left;
				hrlyqunn.move(-playerSpeed,0);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				p2_source.y = Down;
				hrlyqunn.move(0,playerSpeed);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				p2_source.y = Right;
				hrlyqunn.move(playerSpeed,0);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ){
				p2_walkCycle = true;
			} else {
				p2_walkCycle = false;
			}

			////////////////////////////////////////////////////////////////////////////////// Player Collision
			if (zatoichi.getGlobalBounds().intersects(divider.getGlobalBounds()) == true || (zatoichi.getPosition().x + 16) > (divider.getPosition().x - 5) ){
				zatoichi.setPosition((divider.getPosition().x - 21), zatoichi.getPosition().y);
			}
			if (zatoichi.getGlobalBounds().intersects(pad1.getGlobalBounds()) == true){
			
			}
			if (zatoichi.getPosition().x < 0){
				zatoichi.setPosition(0, zatoichi.getPosition().y);
			}
			if (zatoichi.getPosition().y < 66){
				zatoichi.setPosition(zatoichi.getPosition().x, 66);
			}
			if (zatoichi.getPosition().y > 568){
				zatoichi.setPosition(zatoichi.getPosition().x, 568);
			}

			if (hrlyqunn.getGlobalBounds().intersects(divider.getGlobalBounds()) == true || (hrlyqunn.getPosition().x - 16) < (divider.getPosition().x + 5) ){
				hrlyqunn.setPosition((divider.getPosition().x + 21), (hrlyqunn.getPosition().y));
			}
			if (hrlyqunn.getGlobalBounds().intersects(pad1.getGlobalBounds()) == true){

			}
			if (hrlyqunn.getPosition().x > 768){
				hrlyqunn.setPosition(768, hrlyqunn.getPosition().y);
			}
			if (hrlyqunn.getPosition().y < 66){
				hrlyqunn.setPosition(hrlyqunn.getPosition().x, 66);
			}
			if (hrlyqunn.getPosition().y > 568){
				hrlyqunn.setPosition(hrlyqunn.getPosition().x, 568);
			}

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////// Collision
			ball_lastPos = ball.getPosition();
			//ball_nextPos.x = ball_lastPos.x + xVelocityBall;
			//ball_nextPos.y = ball_lastPos.y + yVelocityBall;
			
			///////////////////////////////////////////////////////////////////// Ball Score!!!
			if (ball.getPosition().x < -50){
				pad2Score++;
				ball.setPosition(350,325);
				xVelocityBall = 0;
				yVelocityBall = 0;
				if (ballSpeed < 6){
					ballSpeed++;
				}
			} else if (ball.getPosition().x > 800){
				pad1Score++;
				ball.setPosition(450,325);
				xVelocityBall = 0;
				yVelocityBall = 0;
				if (ballSpeed < 6){
					ballSpeed++;
				}
			}

			//////////////////////////////////////////////////////////////////////////////////////// Pad1
			if (ball.getGlobalBounds().intersects(pad1.getGlobalBounds()) == true){ 
				if (xVelocityBall < 0 && (pad1.getPosition().x < ball.getPosition().x) ){
					xVelocityBall = -xVelocityBall;
				}
				if ( ball_lastPos.y > (pad1.getPosition().y + pad1.getSize().y / 2) ){ //Above the ball
					if (yVelocityBall < 0){
						yVelocityBall = -yVelocityBall;
					}
				} else if ( ball_lastPos.y < (pad1.getPosition().y - pad1.getSize().y / 2) ){ //Below the ball
					if (yVelocityBall > 0){
						yVelocityBall = -yVelocityBall;
					}
				}
				//ball.setPosition(ball_lastPos.x, ball_lastPos.y);
				std::cout << "Pad1" << std::endl;
				hit.play();
			} else if (ball.getGlobalBounds().intersects(pad2.getGlobalBounds()) == true){ ///////// Pad2
				if (xVelocityBall > 0 && (pad2.getPosition().x > ball.getPosition().x) ){
					xVelocityBall = -xVelocityBall;
				}
				if ( ball_lastPos.y > (pad2.getPosition().y + pad2.getSize().y / 2) ){ //Above the ball
					if (yVelocityBall < 0){
						yVelocityBall = -yVelocityBall;
					}
				} else if ( ball_lastPos.y < (pad2.getPosition().y - pad2.getSize().y / 2) ){ //Below the ball
					if (yVelocityBall > 0){
						yVelocityBall = -yVelocityBall;
					}
				}
				//ball.setPosition(ball_lastPos.x, ball_lastPos.y);
				std::cout << "Pad2" << std::endl;
				hit.play();
			}
		
			if (ball.getGlobalBounds().intersects(zatoichi.getGlobalBounds()) == true){ ////////////////////// Zatoichi
				if (zatoichi.getPosition().x < ball.getPosition().x){
				// Right Side
					float difference =  ball.getPosition().x - zatoichi.getPosition().x;
					if (difference > zatoichi.getOrigin().x * 0.84){ //14% per chunk (7 chunks)
						xVelocityBall = ballSpeed;
					} else if (difference > zatoichi.getOrigin().x * 0.56){
						xVelocityBall = int(ballSpeed * 0.7);
					} else if (difference > zatoichi.getOrigin().x * 0.28){
						xVelocityBall = int(ballSpeed * 0.4);
					} else {
						xVelocityBall = int(ballSpeed * 0.1);
					}
				} else {
				//Left Side
					float difference = zatoichi.getPosition().x - ball.getPosition().x;
					if (difference > zatoichi.getOrigin().x * 0.84){ //14% per chunk (7 chunks)
						xVelocityBall = -ballSpeed;
					} else if (difference > zatoichi.getOrigin().x * 0.56){
						xVelocityBall = int(-ballSpeed * 0.7);
					} else if (difference > zatoichi.getOrigin().x * 0.28){
						xVelocityBall = int(-ballSpeed * 0.4);
					} else {
						xVelocityBall = int(-ballSpeed * 0.1);
					}
				}
				if (zatoichi.getPosition().y < ball.getPosition().y){
				// Top Side
					float difference =  ball.getPosition().y - zatoichi.getPosition().y;
					if (difference > zatoichi.getOrigin().y * 0.84){ //14% per chunk (7 chunks)
						yVelocityBall = ballSpeed;
					} else if (difference > zatoichi.getOrigin().y * 0.56){
						yVelocityBall = int(ballSpeed * 0.7);
					} else if (difference > zatoichi.getOrigin().y * 0.28){
						yVelocityBall = int(ballSpeed * 0.4);
					} else {
						yVelocityBall = int(ballSpeed * 0.1);
					}
				} else {
				//Bottom Side
					float difference = zatoichi.getPosition().y - ball.getPosition().y;
					if (difference > zatoichi.getOrigin().y * 0.84){ //14% per chunk (7 chunks)
						yVelocityBall = -ballSpeed;
					} else if (difference > zatoichi.getOrigin().y * 0.56){
						yVelocityBall = int(-ballSpeed * 0.7);
					} else if (difference > zatoichi.getOrigin().y * 0.28){
						yVelocityBall = int(-ballSpeed * 0.4);
					} else {
						yVelocityBall = int(-ballSpeed * 0.1);
					}
				}
				std::cout << "Player 1" << std::endl;
				hit.play();
			}
			if (ball.getGlobalBounds().intersects(hrlyqunn.getGlobalBounds()) == true){ ////////////////////// HrlyQunn
				if (hrlyqunn.getPosition().x < ball.getPosition().x){
				// Right Side
					float difference =  ball.getPosition().x - hrlyqunn.getPosition().x;
					if (difference > hrlyqunn.getOrigin().x * 0.84){ //14% per chunk (7 chunks)
						xVelocityBall = ballSpeed;
					} else if (difference > hrlyqunn.getOrigin().x * 0.56){
						xVelocityBall = int(ballSpeed * 0.7);
					} else if (difference > hrlyqunn.getOrigin().x * 0.28){
						xVelocityBall = int(ballSpeed * 0.4);
					} else {
						xVelocityBall = int(ballSpeed * 0.1);
					}
					std::cout << "Player 2 Right" << std::endl;
				} else {
				//Left Side
					float difference = hrlyqunn.getPosition().x - ball.getPosition().x;
					if (difference > hrlyqunn.getOrigin().x * 0.84){ //14% per chunk (7 chunks)
						xVelocityBall = -ballSpeed;
					} else if (difference > hrlyqunn.getOrigin().x * 0.56){
						xVelocityBall = int(-ballSpeed * 0.7);
					} else if (difference > hrlyqunn.getOrigin().x * 0.28){
						xVelocityBall = int(-ballSpeed * 0.4);
					} else {
						xVelocityBall = int(-ballSpeed * 0.1);
					}
					std::cout << "Player 2 Left" << std::endl;
				}
				if (hrlyqunn.getPosition().y < ball.getPosition().y){
				// Top Side
					float difference =  ball.getPosition().y - hrlyqunn.getPosition().y;
					if (difference > hrlyqunn.getOrigin().y * 0.84){ //14% per chunk (7 chunks)
						yVelocityBall = ballSpeed;
					} else if (difference > hrlyqunn.getOrigin().y * 0.56){
						yVelocityBall = int(ballSpeed * 0.7);
					} else if (difference > hrlyqunn.getOrigin().y * 0.28){
						yVelocityBall = int(ballSpeed * 0.4);
					} else {
						yVelocityBall = int(ballSpeed * 0.1);
					}
					std::cout << "Player 2 Top" << std::endl;
				} else {
				//Bottom Side
					float difference = hrlyqunn.getPosition().y - ball.getPosition().y;
					if (difference > hrlyqunn.getOrigin().y * 0.84){ //14% per chunk (7 chunks)
						yVelocityBall = -ballSpeed;
					} else if (difference > hrlyqunn.getOrigin().y * 0.56){
						yVelocityBall = int(-ballSpeed * 0.7);
					} else if (difference > hrlyqunn.getOrigin().y * 0.28){
						yVelocityBall = int(-ballSpeed * 0.4);
					} else {
						yVelocityBall = int(-ballSpeed * 0.1);
					}
					std::cout << "Player 2 Bottom" << std::endl;
				}
				hit.play();
			}

			//Ball Bounds
			if(ball.getPosition().y < 75){
				if (yVelocityBall < 0){
					yVelocityBall = -yVelocityBall;
				}
				hit.play();
				//ball.setPosition(50, 0);
			} else if (ball.getPosition().y > 575){
				if (yVelocityBall > 0){
					yVelocityBall = -yVelocityBall;
				}
				hit.play();
				//ball.setPosition(50, 500);
			}

			//////// Move
			ball.move(xVelocityBall, yVelocityBall);


			sf::Vector2i mousePosition = sf::Mouse::getPosition(window); //without passing in window it's global screen space (based on your OS)
			//std::cout << "X: " << mousePosition.x << " Y: " << mousePosition.y << std::endl;

			//sf::Mouse::setPosition(sf::Vector2i(400,300), window);

			//////////////////////////////////////////////////////////////////////////////////////////////////// Animation
			frameCounter += frameSpeed * clock.restart().asSeconds();
			
			if (frameCounter >= switchFrame){
				frameCounter = 0;//-= 100; // was setting to 0
				
				//Player 1
				if (p1_walkCycle){
					p1_source.x = walkFrames[p1Frame];
					if (p1Frame < walkFrames.size() - 1){
						p1Frame++;
					} else {
						p1Frame = 0;
					}
				} else {
					p1Frame = 0;
					p1_source.x = walkFrames[p1Frame];
				}

				//Player 2
				if (p2_walkCycle){
					p2_source.x = walkFrames[p2Frame];
					if (p2Frame < walkFrames.size() - 1){
						p2Frame++;
					} else {
						p2Frame = 0;
					}
				} else {
					p2Frame = 0;
					p2_source.x = walkFrames[p2Frame];
				}
			}

			zatoichi.setTextureRect(sf::IntRect(p1_source.x * 32, p1_source.y * 32, 32, 32));
			hrlyqunn.setTextureRect(sf::IntRect(p2_source.x * 32, p2_source.y * 32, 32, 32));

			//////////////RENDERING
			window.clear();

			window.draw(bg);
			window.draw(divider);
			window.draw(pad1);
			window.draw(pad2);
			window.draw(ball);
			window.draw(zatoichi);
			window.draw(hrlyqunn);
		
			//Score
			std::stringstream text;
			text << pad1Score << " : " << pad2Score;
			score.setString(text.str());
			window.draw(score);

			if (pad1Score == scoreToWin){
				winner.setString("Player 1 Wins!");
				gameOver = true;
			}
			if (pad2Score == scoreToWin){
				winner.setString("Player 2 Wins!");
				gameOver = true;
			}
			window.draw(winner);

			window.display();
		}
	}
	// STOP - Game Loop //

	//Clean Up
	window.close();

	return 0;
}