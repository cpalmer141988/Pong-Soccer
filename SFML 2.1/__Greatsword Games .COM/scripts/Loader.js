var Loader = window.loader = {};
Loader.endCount = 0;
Loader.currentCount = 0;

Loader.files_loading = 0;
Loader.files_loaded = 0;

Loader.load = function(js, css, img, callback){
	//The config should be defined in topConfig currently
	var js = js || window.js;
	var css = css || window.css;
	var img = img || window.img;
	Loader.callback = callback;
	Loader.startLoadingProgress(js.length + css.length + img.length);
	Loader.loadJS(js);
	Loader.loadCSS(css);
	Loader.loadIMG(img);
}
 
Loader.loadJS = function(files){
	for (var i=0; i<files.length; i++){
		var js = document.createElement("script");
		js.src = files[i];
		js.onload = function() {
    		Loader.increment_progress();
    		Loader.updateLoadingProgress();
    	}

		document.head.appendChild(js);
		Loader.files_loading++;
	}
}

Loader.loadCSS = function(files){
	for (var i=0; i<files.length; i++){
		var css = document.createElement("link");
		css.rel = "stylesheet";
		css.type = "text/css";
		css.href = files[i];
		css.onload = function() {
    		Loader.increment_progress();
    		Loader.updateLoadingProgress();
    	}

		document.head.appendChild(css);
		Loader.files_loading++;
	}
}

Loader.loadIMG = function(files){
	for (var i=0; i<files.length; i++){
		var file = files[i];
		var id = file,
			index = 0;
		var img = document.createElement("img");
		while (index != -1){
			index = id.indexOf("/");
			if (index != -1){
				id = id.substr(index + 1);
			}
		}
		var extension = id.indexOf(".");
		id = id.substr(0,extension);
		img.id = id; //"images/Characters/Anwen.png"
		img.src = file;
		img.onload = function() {
    		Loader.increment_progress();
    		Loader.updateLoadingProgress();
    	}

		document.head.appendChild(img);
		Loader.files_loading++;
	}
}

//This is used to specifically track when to call finishLoading()
Loader.increment_progress = function(){
	Loader.files_loaded++;
	if (Loader.files_loaded == Loader.files_loading){
		if (Loader.callback) {
			Loader.callback.apply();
		}
	}
}

//Trigger the loading_screen to be visible and start tracking load progress
Loader.startLoadingProgress = function(count){
	Loader.endCount = count;
}

//Update the loading_screen and hide it once we've finished loading
Loader.updateLoadingProgress = function(){
	var ec = this.endCount;
	this.currentCount++;
	var cc = this.currentCount;
	var percent = cc / ec;
	var percentString = (percent.toString().substring(2,4) || 0) + "%";
	
	if (cc == ec){
		this.currentCount = 0;
	}
}
