var PRPG = window.PRPG = {};

PRPG.config = {};
PRPG.config.js = [
	"scripts/objects/PRPG.Units.js",
	"scripts/objects/PRPG.Player.js",
	"scripts/objects/PRPG.Player_actions.js",
	"scripts/objects/PRPG.Battle.js",
	"scripts/objects/PRPG.Battle.board.js",
	"scripts/objects/PRPG.Screen.js",
	"scripts/PRPG.js",

	"scripts/configs/PRPG_lvl_001.js",

	"scripts/MenuManager.js",
	"scripts/GameLoop.js",
	"scripts/Controls.js",


	//"scripts/PRPG_PHYSICS_ENGINE/Grid.js",
	//"scripts/PRPG_PHYSICS_ENGINE/Physics.js",
	//"scripts/PRPG_PHYSICS_ENGINE/Collision.js",

	"scripts/PRPG_GRAPHICS_ENGINE/Renderer_2.js",
	"scripts/PRPG_GRAPHICS_ENGINE/Animator.js"
	/*,
	"scripts/shell/timer.js",
	"scripts/shell/shell.js",
	"scripts/shell/collision.js",
	"scripts/shell/keyboard.js"*/
];

PRPG.config.css = [
	"css/general.css"
];

PRPG.config.img = [
	"images/Characters/Anwen.png",
	"images/Characters/Bloodcrown.png",
	"images/Characters/Findan.png",
	"images/Characters/Ghost.png",
	"images/Characters/Sir Skeleton.png"
];

PRPG.finishLoading = function() {
	window.onresize = PRPG.Screen.windowResize;
	display_menu("main_menu");
}

Loader.load(PRPG.config.js, PRPG.config.css, PRPG.config.img, PRPG.finishLoading);

