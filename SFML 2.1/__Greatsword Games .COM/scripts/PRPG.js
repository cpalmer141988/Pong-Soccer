PRPG.menu_id = null;

PRPG.system_state = "loading";
PRPG.game_state = "Battle"; //NOTE!!! use this state in the game loop to switch between the necessary logic to run each loop

PRPG.objects = {}; //replace this object model for the PRPG
PRPG.levels = {};


PRPG.launch_lvl = function(level){
	var level = PRPG.levels[level]
	PRPG.objects = level.objects;

	PRPG.Screen.setScreenSize();
	display_menu();
	display_game(true);
	game_loop(true);
}