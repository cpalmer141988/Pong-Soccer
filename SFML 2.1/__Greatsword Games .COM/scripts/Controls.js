function startInput(event){
	var key = event.keyCode;//String.fromCharCode(event.keyCode); //turning keyCodes into characters doesn't work for keys such as spacebar, enter, and delete
	var button = event.button;

	//accounting for this in the game_loop now
	if (!PRPG || !PRPG.Battle){
		return false;
	} else {
		var current_player = PRPG.Battle.current_player;
	}

	if (!PRPG.Battle.players || !PRPG.Battle.players[current_player]){
		return false;
	}

	if (PRPG.animating){
		//don't allow user input while animating
		return false;
	}
		
	
	var game = true;
	if (game){
		if (event.type == "mousedown"){
			switch(button){
				case 0:
					var currCommands = PRPG.Battle.players[current_player].currCommands;
					var clickEvent = {
						"type": "left-click",
						"x": event.offsetX,
						"y": event.offsetY
					};
					currCommands.push(clickEvent);
					break;
				case 1:
					break;
				case 2:
					event.preventDefault();
					var currCommands = PRPG.Battle.players[current_player].currCommands;
					var clickEvent = {
						"type": "right-click",
						"x": event.offsetX,
						"y": event.offsetY
					};
					currCommands.push(clickEvent);
					break;
			}
		} else {
			switch(key){
				case 69: //E
				case 83: //S
				case 68: //D
				case 70: //F
					var currCommands = PRPG.Battle.players[current_player].currCommands;
					var existance=0;
					for (var i=0; i<currCommands.length; i++){
						if (currCommands[i] == key) {return;}
						else {existance++;}
					}
					if (existance == currCommands.length){
						currCommands.push(key);
					}
					break;

				case 16: //shift
					break;
				case 17: //ctrl
					//event.preventDefault(); //need to override ctrl+shift+j somehow, ctril alone doesn't prevent the debugger from opening
					break;
				case 18: //alt
					break;
				case 32: //space
					var currCommands = PRPG.Battle.players[current_player].currCommands;
					var existance=0;
					for (var i=0; i<currCommands.length; i++){
						if (currCommands[i] == key) {return;}
						else {existance++;}
					}
					if (existance == currCommands.length){
						currCommands.push(key);
					}
					break;
				case 13: //enter
					break;
				case 8:  //backspace
					break;
				case 46: //delete
					break;
				case 123: //F12
					//event.preventDefault();
					break;

				case "q": //q
					break;
				case "e": //e
					break;
				case "r": //r
					break;
				case "f": //f
					break;

				case "0": //0
					break;
				case "1": //1
					break;
				case "2": //2
					break;
				case "3": //3
					break;
				case "4": //4
					break;
				case "5": //5
					break;
				case "6": //6
					break;

				default:
					break;
			}
		}
	}
}

function stopInput(event){
	var key = event.keyCode;//String.fromCharCode(event.keyCode);  //turning keyCodes into characters doesn't work for keys such as spacebar, enter, and delete
	
	//accounting for this in the game_loop now
	if (!PRPG || !PRPG.Battle){
		return false;
	} else {
		var current_player = PRPG.Battle.current_player;
	}

	if (!PRPG.Battle.players || !PRPG.Battle.players[current_player]){
		return false;
	}

	if (PRPG.animating){
		//prevent actions from being lost during animation
		return false;
	}

	var game = true;
	if (game){
		switch(key){
			case 69: //E
			case 83: //S
			case 68: //D
			case 70: //F
				var currCommands = PRPG.Battle.players[current_player].currCommands;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) { currCommands.splice(i,1); }
				}
				break;

			case 16: //shift
				break;
			case 32: //space
				var currCommands = PRPG.Battle.players[current_player].currCommands;
				for (var i=0; i<currCommands.length; i++){
					if (currCommands[i] == key) { currCommands.splice(i,1); }
				}
				break;
			case 13: //enter
				break;
			case 8:  //backspace
				break;
			case 46: //delete
				break;

			case "q": //q
				break;
			case "e": //e
				break;
			case "r": //r
				break;
			case "f": //f
				break;

			case "0": //0
				break;
			case "1": //1
				break;
			case "2": //2
				break;
			case "3": //3
				break;
			case "4": //4
				break;
			case "5": //5
				break;
			case "6": //6
				break;

			default:
				break;
		}
	}
}

function execPlayerCommands(current_player){
	//accounting for this in the game_loop now
	// if (!PRPG || !PRPG.Battle){
	// 	return false;
	// } else {
	// 	var current_player = PRPG.Battle.current_player;
	// }

	// if (!PRPG.Battle.players || !PRPG.Battle.players[current_player]){
	// 	return false;
	// }

	var currCommands = PRPG.Battle.players[current_player].currCommands;
	for (var i=0; i<currCommands.length; i++){
		var command = currCommands[i];
		if (command.type == "left-click"){
			var x = command.x;
			var y = command.y;
			//console.log("Left-Click: " + x +" "+ y);
			PRPG.Battle.players[current_player].left_click(x, y);

			PRPG.Battle.players[current_player].currCommands = extract(currCommands, i);
		} else if (command.type == "right-click"){
			var x = command.x;
			var y = command.y;
			//console.log("Right-Click: " + x +" "+ y);
			PRPG.Battle.players[current_player].right_click(x, y);

			PRPG.Battle.players[current_player].currCommands = extract(currCommands, i);
		} else {
			switch(command){
				case 69: //E
					//translate_object(PRPG.objects["player_001"], {"y":-1}, 100, PRPG.delta_time);
					break;
				case 83: //S
					//translate_object(PRPG.objects["player_001"], {"x":-1}, 100, PRPG.delta_time);
					break;
				case 68: //D
					//translate_object(PRPG.objects["player_001"], {"y":1}, 100, PRPG.delta_time);
					break;
				case 70: //F
					//translate_object(PRPG.objects["player_001"], {"x":1}, 100, PRPG.delta_time);
					break;
				case 32: //space
					PRPG.Battle.players[current_player].add_reinforcements();
					break;
				default:
					break;
			}
		}
	}

	//var t=setTimeout("execPlayerCommands()",10);
}