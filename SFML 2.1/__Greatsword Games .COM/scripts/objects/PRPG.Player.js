PRPG.Player = function(){
	this.id = "";
	this.control = "";
	this.currCommands = [];

	this.portrait = "";
	this.level = 10;
	this.maxHP = 100;
	this.HP = 100;
	this.maxMP = 100;
	this.MP = 100;

	//relate mana on the board to mana for spells?

	this.placement = "";
	this.max_mana = 30;
	this.mana = 0;
	this.summoning_slots = 0;
	this.mana_origin = "";
	this.mana_destination = "";
	this.columnData = [];
	this.reinforcements = {
		"soldier":  [ {}, {}, {} ], //3
		"champion": [ {}, {} ],     //2
		"legend":   [ {} ]          //1
	};
	this.units = [];

	this.max_actions = 4;
	this.remaining_actions = 0;
	this.current_action = "idle";
	this.held_mana = "";
}

PRPG.Player.prototype.add_mana = function(column, row, color){
	var placement = this.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	this.mana++;
	board[column][row] = {"type":"mana", "color":color};
}

PRPG.Player.prototype.swap_mana = function(column, row){
	var placement = this.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	var color = this.held_mana;
	var target_color = board[column][row].color;

	var mana_origin = this.mana_origin;
	var column_origin = mana_origin.column;
	var row_origin = mana_origin.row;

	if (color == target_color){
		//you don't want to do that... that'd be dumb
		return false;
	} else if (row_origin != row){
		//we're not allowing vertical swaps as it seems to take away the point of deleting slots
		return false;
	} else if (column_origin != column && row_origin != row){
		//this is a diagonal swap, and isn't allowed
		return false;
	} else if (column_origin < column - 1 || column_origin > column + 1){
		//not in range horizontally
		return false;
	// } else if (row_origin < row - 1 || row_origin > row + 1){
	// 	//not in range vertically
	// 	return false;
	} else {
		//lets get our switch on!
		this.held_mana = "";
		this.current_action = "idle";
		board[column_origin][row_origin].color = target_color;
		board[column][row].color = color;
	}
	
	this.use_action_point();	
}

PRPG.Player.prototype.pickup_mana = function(column, row, color){
	var placement = this.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	this.mana_origin = {"column":column, "row":row, "color":color};
	this.held_mana = color;
	this.current_action = "holding_mana";
	board[column][row].color = "grey";
}

PRPG.Player.prototype.drop_mana = function(column, row){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data
	var mana_origin = this.mana_origin;

	if (mana_origin.column != column){
		var columnData = Battle.board.get_columnData();
		var lowest_row_available = 6 - columnData[column];
		if (row > lowest_row_available){
			row = lowest_row_available;
		}

		//drop the mana in the correct slot
		var color = this.held_mana;
		this.mana_destination = {"column":column, "row":row, "color":color};

		this.held_mana = "";
		this.current_action = "idle";
		//make sure we update column data to reflect the changes in each column
		//this.columnData[mana_origin.column]++;
		//this.columnData[column]--;
		board[column][row].color = color;
		//clear the mana_origin block
		board[mana_origin.column][mana_origin.row] = {};
		
		this.use_action_point();
	} else {
		this.mana_destination = mana_origin;
		column = mana_origin.column;
		row = mana_origin.row;
		var color = mana_origin.color;

		this.held_mana = "";
		this.current_action = "idle";
		board[column][row].color = color;
	}
}

PRPG.Player.prototype.clear_slot = function(column, row, type){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	board[column][row] = {};
	//this.columnData[column]++;
	this.mana--;
	Battle.board.gravitate_to_center(column); 
}

PRPG.Player.prototype.use_action_point = function(){
	this.remaining_actions--;
	//everytime an action point was used, something has shifted, lets check for new formations
	PRPG.Battle.board.check_for_formations();
}

PRPG.Player.prototype.gain_action_point = function(){
	this.remaining_actions++;
}



PRPG.Player.prototype.create_wall = function(column, row, formationData){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data

	//board[column][row] = new ???;
	this.clear_slot(column, formationData.row, formationData.type);
	Battle.board.shift_back(column, row); //move mana back while we have reference to the spot the unit will go in
	board[column][row] = {
		"type": "wall",
		"multiplier": formationData.multiplier,
		"toughness": 5 * formationData.multiplier
	};
}

PRPG.Player.prototype.update_wall = function(column, row, changeData){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data

	//board[column][row] = new ???;
	var wall = board[column][row];
	wall.toughness = 5 * wall.multiplier;
}

PRPG.Player.prototype.create_summoning_slot = function(column, row, formationData){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data

	//board[column][row] = new ???;
	for (var i=0; i<formationData.combo; i++){
		//for each mana used to create this formation, delete it
		this.clear_slot(formationData.column, formationData.row, formationData.type);
	}				
	Battle.board.shift_back(column, row); //move mana back while we have reference to the spot the unit will go in
	board[column][row] = {
		"type": "summoning_slot",
		"summon_size": "soldier",
		"multiplier": formationData.multiplier
	};
	this.summoning_slots++;
}

PRPG.Player.prototype.show_summons = function(column, row, summon_size){
	switch (summon_size){
		case "legend":
			//do stuff and fall through the rest
		case "champion":
			//do stuff and fall through the rest
		case "soldier":
			//do stuff and finish
			break;
	}

	this.reinforcements[summon_size];
}

PRPG.Player.prototype.summon_unit = function(column, row, unit){
	
}