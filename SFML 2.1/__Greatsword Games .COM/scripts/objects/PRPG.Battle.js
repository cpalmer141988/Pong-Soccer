PRPG.Battle = {
	"player_count": 2,
	"players": {},
	"current_player": false,
	"current_player_number": false
};

PRPG.Battle.start_battle = function(){
	this.board.create_board();
	//for each player call player.add_reinforcements ( use prototypes for the player objects =D )
	var players = PRPG.Battle.player_count;

	for (var i=1; i<=players; i++){
		PRPG.Battle.players["player_"+i] = inherit(PRPG.Player);
		PRPG.Battle.players["player_"+i].id = "player_"+i;
	}
	switch(players){
		case 2:
			PRPG.Battle.players["player_2"].placement = "TOP";
			PRPG.Battle.players["player_2"].portrait = "Ghost";
			PRPG.Battle.players["player_1"].placement = "BOTTOM";
			PRPG.Battle.players["player_1"].portrait = "Sir Skeleton";
			break;
		case 3:
			break;
		case 4:
			break;
	}

	this.first_turn();
}

PRPG.Battle.first_turn = function(){
	var players = PRPG.Battle.players;
	for (var i in players){
		var player = players[i];
		this.current_player = player.id;
		player.add_reinforcements(true);
	}

	//Pick a random player to go first
	var roll_for_first = Math.floor(Math.random()*PRPG.Battle.player_count+1);
	this.start_turn(PRPG.Battle.players["player_"+roll_for_first], roll_for_first);
}

PRPG.Battle.start_turn = function(player, number){
	this.current_player = player.id;
	this.current_player_number = number;
	player.remaining_actions = player.max_actions;
	/*//check for debuffs
	if (player.debuffs.length > 0){
		//tick down debuff durations
	}

	//charging_unit timers tick down
	if (player.charging_units.length > 0){
		var charging_units = player.charging_units;
		var sent_units = [];

		for (var i=0; i<charging_units.length; i++){
			var unit = charging_units[i];
			var charge_timer = unit.charge_timer;

			//can trigger attacks to go off
			if (charge_timer == 0){
				sent_units.push(i);
				send_attack(unit);
			} else {
				charge_timer--;
			}
		}

		//if charge formations went off, we should have a list of units to remove from the charging_units array
		if (sent_units.length){
			for (var i=0; i<sent_units.length; i++){
				player.charging_units = extract(charging_units,sent_units[i]);
			}
		}
	}

	//Will go up and down based on actions taken and the results of those actions
	//This could also be tracked as Action Points (AP) or some type of stamina stat
	player.moves_remaining = player.moves_stat;

	//start turn timer (if enabled)
	turn_timer(game.turn_length);*/
}

PRPG.Battle.end_turn = function(){
	var player = this.players[this.current_player];

	if (player.remaining_actions == 0){
		var players = this.players;
		var next_player_number = (this.current_player_number + 1 > this.player_count) ? 1 : this.current_player_number + 1;
		//end the players turn
		this.start_turn(PRPG.Battle.players["player_"+next_player_number], next_player_number);
	}

	//turn isn't over yet
	return false;
}
