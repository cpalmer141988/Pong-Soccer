PRPG.Player.prototype.add_reinforcements = function(game_start){
	var reinforcement_count = this.max_mana - this.mana;

	if (reinforcement_count > 0){
		var id = this.id;

		//create a count of available rows per column
		var board = PRPG.Battle.board;
		var columnData = board.get_columnData();

		for (var i=0; i<reinforcement_count; i++){
			var column = board.rand_columnDrop(columnData);
			var row = 6 - columnData[column]; //Based on spots available, push the drop to the front most row

			var color = board.select_dropColor(column, row);
			if (!color){
				i--;
				continue;
			}
			this.add_mana(column, row, color);
			columnData[column]--; //need to keep this updated so it places them properly
		}
	} else {
		return false;
	}

	if (!game_start){
		this.use_action_point();
	}
}

PRPG.Player.prototype.left_click = function(x, y){
	var Battle = PRPG.Battle;
	var placement = this.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data

	if ( Battle.board.is_inBounds(placement, x, y) ){
		var column = Battle.board.get_column(x);
		var row = Battle.board.get_row(placement, y);

		switch(this.current_action){
			case "idle":
				//we check if there's something to pickup
				var slot = board[column][row];
				var type = slot.type;
				switch(type){
					case "mana":
						var color = slot.color;
						this.pickup_mana(column, row, color);
						break;
					case "summoning_slot":
						this.show_summons(column, row, slot.summon_size);
						break;
				}
				break;
			case "holding_mana":
				//we check to see if they can drop it here
				var mana_origin = this.mana_origin;
				var swap_only = false,
					columnData = Battle.board.get_columnData(),
					mana_origin_lra = 6 - columnData[mana_origin.column];
				if (mana_origin.row < mana_origin_lra - 1){
					//this can only be swapped, it can't be dropped in other rows
					swap_only = true;
				}

				if (mana_origin.column == column && mana_origin.row == row){
					//we're returning the mana to the original slot
					this.drop_mana(column, row);
				} else if (!swap_only && board[column][row].type !== "mana" && board[column][row].color != "grey"){
					//lets drop the mana in a different column
					this.drop_mana(column, row);
				} else if (board[column][row].type === "mana"){
					//lets try swapping with an adjacent slot
					this.swap_mana(column, row);
				} else if (mana_origin.column == column){
					//return to orginal position
					this.drop_mana(column, row);
				} else {
					//THIS WON'T EVER BE CALLED WITH SWAPPING AS AN OPTION
					//can't place here, occupied by mana already
					return false; //don't think we need to do anything for this atm, maybe add a beep or something later...
				}
				break;
			case "summoning":
				break;
		}
	}
}

PRPG.Player.prototype.right_click = function(x, y){
	var placement = this.placement; //Grab what side of the board this player is on
	var Battle = PRPG.Battle;
	var board = Battle.board[placement]; //Grab that half of the boards grid data
	
	if ( Battle.board.is_inBounds(placement, x, y) ){
		var column = Battle.board.get_column(x);
		var row = Battle.board.get_row(placement, y);
		var type = board[column][row].type;

		switch(this.current_action){
			case "idle":
				//we check if there's something to pickup
				if (type == "mana" || type == "wall" || type == "summoning_slot"){
					this.clear_slot(column, row, type);
					this.use_action_point();
				}
				break;
			case "holding_mana":
				//the user is holding mana to drop, don't let them delete until it's dropped
				return false;
				break;
		}
	}
}