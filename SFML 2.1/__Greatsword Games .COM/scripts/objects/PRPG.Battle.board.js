PRPG.Battle.board = {
	"x": {},
	"y": {},
	"w": {},
	"h": {},
	"slot_size": {},
	"divider_height": {},
	"TOP": [],
	"BOTTOM": []
}

PRPG.Battle.board.create_board = function(){
	for (var i=0; i<8; i++){
		PRPG.Battle.board["TOP"].push([]);
		PRPG.Battle.board["BOTTOM"].push([]);

		for (var j=0; j<6; j++){
			PRPG.Battle.board["TOP"][i].push({});
			PRPG.Battle.board["BOTTOM"][i].push({});
		}
	}
}

PRPG.Battle.board.is_inBounds = function(side, x, y){
	var boardData = PRPG.Screen.get_boardSizeData();
	var slot_size = boardData.slot_size,
		left_edge = boardData.left_edge;

	switch(side){
		case "TOP":
			var Ty_0 = slot_size;
			if (y >= Ty_0 && y <= Ty_0 + (slot_size * 6) && x >= left_edge && x <= left_edge + (slot_size * 8)){
				return true;
			}
			break;
		case "BOTTOM":
			var By_0 =  slot_size * 8;
			if (y >= By_0 && y <= By_0 + (slot_size * 6) && x >= left_edge && x <= left_edge + (slot_size * 8)){
				return true;
			}
			break;
	}

	return false;
}

PRPG.Battle.board.get_column = function(x){
	var boardData = PRPG.Screen.get_boardSizeData();
	var slot_size = boardData.slot_size,
		column_count = boardData.column_count;

	var column = Math.floor((x / slot_size) - ((column_count - 8) / 2));
	return column;
}

PRPG.Battle.board.get_row = function(side, y){
	switch(side){
		case "TOP":
			var row = (6 - Math.floor(y / PRPG.Screen.boardData.slot_size));
			return row;
		case "BOTTOM":
			var row = (Math.floor(y / PRPG.Screen.boardData.slot_size) - 8);
			return row;
	}
}

PRPG.Battle.board.get_columnData = function(player){
	// if (this.columnData.length > 0){
	// 	var columnData = this.columnData;
	// 	return columnData;
	// } else {
	var columnData = []; //I think it's safer to recalculate this each time it's needed than expect the logic to keep it properly updated
	// }

	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	var rows = board[0].length;
	var columns = board.length;
	
	for (var i=0; i<columns; i++){
		var available_slots = 0;

		for(var j=rows-1; j>=0; j--){
			var slot = board[i][j];
			if (!slot.type){
				available_slots++;
			}
		}

		columnData.push(available_slots);
	}

	//column_availability ex. [2,2,1,3,4,0,4,2,6,5]
	//numbers should range from 0 - 6
	return columnData;
}

PRPG.Battle.board.rand_columnDrop = function(columnData){
	//The more available slots in a column the more entries that'll be in the pool for selection to add to that row
	var pool = [];
	for (var i=0; i<10; i++){
		for (var j=0; j<columnData[i]; j++){
			pool.push(i);
		}
	}

	//lets make a random selection from the pool
	var rand = Math.floor((Math.random()*pool.length));
	var column = pool[rand];

	return column;
}

PRPG.Battle.board.select_dropColor = function(column, row){
	//temporary
	var rand = Math.floor((Math.random()*3)+1);
	var valid_drop = false;
	var disable_color_1 = false;
	var disable_color_2 = false;
	var disable_color_3 = false;

	while (!valid_drop){
		if (disable_color_1 && disable_color_2 && disable_color_2){
			return false;
		} else if (disable_color_1 && disable_color_2){
			rand = 3
		} else if (disable_color_1 && disable_color_3){
			rand = 2
		} else if (disable_color_2 && disable_color_3){
			rand = 1
		} else if (disable_color_1){
			rand = Math.floor((Math.random()*2)+1);
			if (rand == 1){
				rand = 2;
			} else {
				rand = 3;
			}
		} else if (disable_color_2){
			rand = Math.floor((Math.random()*2)+1);
			if (rand == 1){
				rand = 1;
			} else {
				rand = 3;
			}
		} else if (disable_color_3){
			rand = Math.floor((Math.random()*2)+1);
		}
		
		switch(rand){
			case 1:
				var color = "green";
				valid_drop = this.validate_drop(column, row, color);

				if (!valid_drop){
					disable_color_1 = true;
				}
				break;
			case 2:
				var color = "blue";
				valid_drop = this.validate_drop(column, row, color);

				if (!valid_drop){
					disable_color_2 = true;
				}
				break;
			case 3:
				var color = "red";
				valid_drop = this.validate_drop(column, row, color);

				if (!valid_drop){
					disable_color_3 = true;
				}
				break;
		}
	}
	

	return color;
}

PRPG.Battle.board.validate_drop = function(column, row, color){
	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	var valid_drop = false;

	if (!((column>0 && (board[column-1][row].color==color)) && (column<7 && (board[column+1][row].color==color)))){
		//cleared 1st check - columns to the left and right

		if (!(row>=2 && (board[column][row-1].color==color) && (board[column][row-2].color==color))){
			//cleared 2nd check - rows below

			if (!(column>1 && board[column-1][row].color==color && board[column-2][row].color==color)){
				//cleared 3rd check - columns to the left

				if (!(column<6 && board[column+1][row].color==color && board[column+2][row].color==color)){
					//cleared 4th check - columns to the right
					valid_drop = true;
				}
			}
		}
	}

	return valid_drop;
}

PRPG.Battle.board.check_for_formations = function(){
	//we should check this everytime an action point is used
	//and everytime an attack formations goes off
	//and everytime units or mana are destroyed by attacking formations

	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	var formations = {
		"wall_formations": [],
		"summoning_slots": []
	};

	var formations_found = 0;

	var vert_color = false,
		vert_count = 0;
	var hori_color = false,
		hori_count = 0;

	//NEED TO IMPROVE THESE TO TRACK WHICH SLOTS ARE PART OF THE FORMATION TO BE PASSED INTO THE CREATE_FORMATION FUNCTION!

	//check columns (vert)
	outer_vert:
	for (var i=0; i<8; i++){
		var vert_color = false,
			vert_count = 0;

		for (var j=0; j<6; j++){
			slot = board[i][j];

			if (j == 0){
				//first slot in the column
				if (slot.type === "mana"){
					//lets start matching for this column
					vert_color = slot.color;
					vert_count = 1;
				} else {
					//this column is empty
					vert_count = 0;
					continue outer_vert;
				}
			} else if (vert_color == slot.color){
				//lets increment the match count
				vert_count++;
				if (j == 5){
					//this is the last match of the column
					if (vert_count >= 3){
						switch(vert_count){
							case 3:
								formations.summoning_slots.push({
									"type":"summoning_slot",
									"column":i,
									"row":j-2,
									"combo":3,
									"multiplier":1
								});
								formations_found++;
								break;
							case 4:
								formations.summoning_slots.push({
									"type":"summoning_slot",
									"column":i,
									"row":j-3,
									"combo":4,
									"multiplier":1.5
								});
								formations_found++;
								break;
							case 5:
								formations.summoning_slots.push({
									"type":"summoning_slot",
									"column":i,
									"row":j-4,
									"combo":5,
									"multiplier":2
								});
								formations_found++;
								break;
						}

						vert_color = false;
						vert_count = 0;
					} else {
						//we didn't have enough matches remaining to create another formation
						vert_color = false;
						vert_count = 0;
					}
				}
			} else if (vert_count >= 3){
				//not a match but we have enough for a formation
				switch(vert_count){
					case 3:
						formations.summoning_slots.push({
							"type":"summoning_slot",
							"column":i,
							"row":j-3,
							"combo":3,
							"multiplier":1
						});
						formations_found++;
						break;
					case 4:
						formations.summoning_slots.push({
							"type":"summoning_slot",
							"column":i,
							"row":j-4,
							"combo":4,
							"multiplier":1.5
						});
						formations_found++;
						break;
					case 5:
						formations.summoning_slots.push({
							"type":"summoning_slot",
							"column":i,
							"row":j-5,
							"combo":5,
							"multiplier":2
						});
						formations_found++;
						break;
				}

				if (typeof slot === "string"){
					vert_color = slot.color;
				} else {
					vert_color = false;
				}
				vert_count = 0;
			} else {
				//not a match and not enough for a formation
				if (slot.type === "mana"){
					//lets start matching for this column
					vert_color = slot.color;
					vert_count = 1;
				} else {
					//this column is empty
					vert_color = false;
					vert_count = 0;
				}
			}
		}
	}
	
	//check rows (hori) - switching i and j to make this work
	outer_hori:
	for (var j=0; j<6; j++){
		var hori_color = false,
			hori_count = 0;

		for (var i=0; i<8; i++){
			slot = board[i][j];

			if (i == 0){
				//first slot in the column
				if (slot.type === "mana"){
					//lets start matching for this column
					hori_color = slot.color;
					hori_count = 1;
				} else {
					//this column is empty
					hori_count = 0;
					continue outer_hori;
				}
			} else if (hori_color == slot.color){
				//lets increment the match count
				hori_count++;
				if (i == 7){
					//this is the last match of the column
					if (hori_count >= 3){
						switch(hori_count){
							case 3:
								formations.wall_formations.push({
									"type":"wall",
									"column":i-2,
									"row":j,
									"combo":3,
									"multiplier":1
								});
								formations_found++;
								break;
							case 4:
								formations.wall_formations.push({
									"type":"wall",
									"column":i-3,
									"row":j,
									"combo":4,
									"multiplier":1.5
								});
								formations_found++;
								break;
							case 5:
								formations.wall_formations.push({
									"type":"wall",
									"column":i-4,
									"row":j,
									"combo":5,
									"multiplier":1
								});
								formations_found++;
								break;
						}

						hori_color = false;
						hori_count = 0;
					} else {
						//we didn't have enough matches remaining to create another formation
						hori_color = false;
						hori_count = 0;
					}
				}
			} else if (hori_count >= 3){
				//not a match but we have enough for a formation
				switch(hori_count){
					case 3:
						formations.wall_formations.push({
							"type":"wall",
							"column":i-3,
							"row":j,
							"combo":3,
							"multiplier":1
						});
						formations_found++;
						break;
					case 4:
						formations.wall_formations.push({
							"type":"wall",
							"column":i-4,
							"row":j,
							"combo":4,
							"multiplier":1.5
						});
						formations_found++;
						break;
					case 5:
						formations.wall_formations.push({
							"type":"wall",
							"column":i-5,
							"row":j,
							"combo":5,
							"multiplier":2
						});
						formations_found++;
						break;
				}

				if (slot.type === "mana"){
					hori_color = slot.color;
				} else {
					hori_color = false;
				}
				hori_count = 0;
			} else {
				//not a match and not enough for a formation
				if (slot.type === "mana"){
					//lets start matching for this column
					hori_color = slot.color;
					hori_count = 1;
				} else {
					//this column is empty
					hori_color = false;
					hori_count = 0;
				}
			}
		}
	}

	if (formations_found){
		this.create_formations(formations);
	}
}

PRPG.Battle.board.create_formations = function(formations){
	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data
	
	var index = ["wall_formations", "summoning_slots"]; //this lets us process things based on order or importance, or in other words, what will be in front of what

	for (var i=0; i<index.length; i++){
		var formation_type = formations[index[i]];

		for (var j=0; j<formation_type.length; j++){
			var formation = formation_type[j],
				type = formation.type,
				column = formation.column,
				row = formation.row,
				combo = formation.combo,
				multiplier = formation.multiplier;

			switch(type){
				case "wall":
					outer_loop:
					for (var k=column; k<column+combo; k++){
						inner_loop:
						for (var m=0; m<6; m++){
							var slot = board[k][m];
							if (multiplier){
								switch(slot.type){
									case "wall":
										multiplier = this.combine_formations(slot, formation);
										player.update_wall(k, m);
										if (multiplier){
											continue inner_loop;
										} else {
											continue outer_loop;
										}
									default: //not a formation, could be a mana block or empty slot, either way we're going to move this		
										player.create_wall(k, m, formation); //create the wall in the now empty slot
										continue outer_loop;
								}
							}
						}
					}
					break;
				case "summoning_slot":
					loop:
					for (var m=0; m<6; m++){
						var slot = board[column][m];
						if (multiplier){
							switch(slot.type){
								case "wall":
									continue;
									break;
								case "summoning_slot":
									//multiplier = this.combine_formations(slot, formation);
									break;
								default: //not a formation, could be a mana block or empty slot, either way we're going to move this
									player.create_summoning_slot(column, m, formation); //create the unit in the now empty slot
									break loop;
							}
						}
					}
					break;
				default:
					break;
			}
		}
	}

	//now
	//this.check_for_formations();
}

PRPG.Battle.board.combine_formations = function(origin, formation){
	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];

	switch(origin.multiplier){
		case 0.5:
			if (formation.multiplier > 1.5){ //combine the walls
				origin.multiplier = 2;
				formation.multiplier -= 1.5;
			} else {
				origin.multiplier = 0.5 + formation.multiplier;
				formation.multiplier = 0;

				//we full combined so we can start shifting mana from behind the formation up
				player.clear_slot(formation.column, formation.row, formation.type);
			}
			break;
		case 1:
			if (formation.multiplier > 1){ //combine the walls
				origin.multiplier = 2;
				formation.multiplier -= 1;
			} else {
				origin.multiplier = 1 + formation.multiplier;
				formation.multiplier = 0;

				//we full combined so we can start shifting mana from behind the formation up
				player.clear_slot(formation.column, formation.row, formation.type);
			}
			break;
		case 1.5:
			if (formation.multiplier > 0.5){ //combine the walls
				origin.multiplier = 2;
				formation.multiplier -= 0.5;
			} else {
				origin.multiplier = 1.5 + formation.multiplier;
				formation.multiplier = 0;

				//we full combined so we can start shifting mana from behind the formation up
				player.clear_slot(formation.column, formation.row, formation.type);
			}
			break;
		case 2:
			break;
	}

	return formation.multiplier;
}

PRPG.Battle.board.gravitate_to_center = function(column){
	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = Battle.board[placement]; //Grab that half of the boards grid data

	if (column){
		outter_loop:
		for (var j=0; j<5; j++){ //we don't need to the last row, because there would be nothing to pull down
			var slot = board[column][j];

			if (!slot.type){
				for (var k=(j+1); k<6; k++){
					var target = board[column][k];

					if (target.type){
						board[column][j] = target; //assign the target to the slot
						board[column][k] = {}; //clear the target slot
						continue outter_loop;
					} else if (k == 5){
						break outter_loop; //we're not going to find anything else to pull down, lets end the search
					}
				}
			}
		}
	} else {
		outter_loop:
		for (var i=0; i<7; i++){
			inner_loop:
			for (var j=0; j<5; j++){ //we don't need to the last row, because there would be nothing to pull down
				var column = i;
				var slot = board[column][j];

				if (!slot.type){
					for (var k=(j+1); k<6; k++){
						var target = board[column][k];

						if (target.type){
							board[column][j] = target; //assign the target to the slot
							board[column][k] = {}; //clear the target slot
							continue inner_loop;
						} else if (k == 5){
							continue outter_loop; //we're not going to find anything else to pull down, lets end the search
						}
					}
				}
			}
		}
	}
}

PRPG.Battle.board.shift_back = function(column, row){
	//shifts mana up/down when formations happen

	var Battle = PRPG.Battle;
	var player = Battle.players[Battle.current_player];
	var placement = player.placement; //Grab what side of the board this player is on
	var board = PRPG.Battle.board[placement]; //Grab that half of the boards grid data

	var lastSlot = board[column][row];
	for (var j=row+1; j<6; j++){
		var slot = board[column][j];
		board[column][j] = lastSlot;
		lastSlot = slot;
	}
}