PRPG.Screen = {
	"id": "loading_screen"
}

PRPG.Screen.setScreenSize = function(){
	var screenSize = getScreenSize(window.windowSize, window.aspectRatio);
	PRPG.Screen.width = screenSize[0];
	PRPG.Screen.height = screenSize[1];
	var screen_width = PRPG.Screen.width;
	var screen_height = PRPG.Screen.height;

	setSize("content", screenSize);
	set_canvas_resolution("background",screen_width,screen_height);
	set_canvas_resolution("foreground",screen_width,screen_height);
	set_canvas_resolution("hud",screen_width,screen_height);
}

PRPG.Screen.windowResize = function(){
	this.resized = true;
	
	window.windowSize = getWindowSize();
	window.aspectRatio = getAspectRatio(windowSize);
	var screenSize = getScreenSize(window.windowSize, window.aspectRatio);
	PRPG.Screen.width = screenSize[0];
	PRPG.Screen.height = screenSize[1];
	var screen_width = PRPG.Screen.width;
	var screen_height = PRPG.Screen.height;

	setSize("content", screenSize);
	set_canvas_resolution("background",screen_width,screen_height);
	set_canvas_resolution("foreground",screen_width,screen_height);
	set_canvas_resolution("hud",screen_width,screen_height);
}

PRPG.Screen.get_boardSizeData = function(){
	var boardData = this.boardData;
	if (boardData){
		return boardData;
	}

	this.boardData = {};
	boardData = this.boardData;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	boardData.slot_size = screen_height / 15;
	var slot_size = boardData.slot_size;
	boardData.column_count =  Math.round(screen_width / slot_size);
	boardData.left_edge = ( (boardData.column_count - 8) * slot_size) / 2;

	return boardData;
}