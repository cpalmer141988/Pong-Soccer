function game_loop(first_loop){
    if (!PRPG.init_time) {
        var init_time = PRPG.init_time = (new Date()).getTime(),
            last_time = PRPG.last_time,
            curr_time = PRPG.curr_time,
            delta_time = PRPG.delta_time,

            total_seconds_played = PRPG.total_seconds_played,
            
            fps_start = PRPG.fps_start,
            fps_end = PRPG.fps_end,
            fps_avg = PRPG.fps_avg,
            fps_curr = PRPG.fps_curr,

            frames_last_second = PRPG.frames_last_second,
            frames_this_second = PRPG.frames_this_second,

            last_frame = PRPG.last_frame,
            curr_frame = PRPG.curr_frame;
    }

    var state = PRPG.state;

    if (state != "quit" && state != "pause") {
        PRPG.last_time = PRPG.curr_time || PRPG.init_time - PRPG.init_time;
        PRPG.curr_time = (new Date()).getTime() - PRPG.init_time;

        PRPG.total_seconds_played = (PRPG.curr_time - PRPG.init_time) / 1000;
        if (!PRPG.fps_start || PRPG.curr_time >= PRPG.fps_end) {
            PRPG.fps_start = PRPG.fps_end || PRPG.curr_time;
            PRPG.fps_end = PRPG.fps_start + 1000;
            PRPG.frames_last_second = PRPG.frames_this_second;
            PRPG.frames_this_second = 0;
        }

        PRPG.delta_time = (PRPG.curr_time - PRPG.last_time) / 1000;

        //INIT
        //Perform initiation operations here!
        if (first_loop){
            //update_current_position(PRPG.objects);
            PRPG.Battle.start_battle();
        }

        if (!first_loop) { //was PRPG.delta_time
            if (!PRPG.animating){
                if (PRPG.Battle.current_player){
                    //Check for end of turn
                    var Battle = PRPG.Battle,
                        player = Battle.current_player;
                    Battle.end_turn(player);
                    Battle.board.check_for_formations();//lets try redetecting formations at the start of each loop?
                    //PLAYER INPUT
                    execPlayerCommands(Battle.current_player, PRPG.delta_time);
                    //AI
                    //PHYSICS
                }
            }

            //PRPG.detection = collision_check_group(PRPG.objects);

        //RENDER
            //if we want to re-render the background
            //...

            //if we want to re-render the foreground
            clear_canvas("foreground");
            render_board("foreground", PRPG.Battle.board);
            //render_group("foreground", PRPG.objects);

            //render_collisions("hud",PRPG.detection.collisions);
            //render_non_collisions("hud",PRPG.detection.non_collisions);

            //if we want to re-render the hud
            //...
            clear_canvas("hud");
            //render_collisions("hud",PRPG.detection.collisions);
            //render_non_collisions("hud",PRPG.detection.non_collisions);
            render_player_guis("hud", PRPG.Battle.players);
            render_fps("hud",PRPG.frames_last_second);
        }

        PRPG.frames_this_second++;
        setTimeout(function() { game_loop(); }, 1);
    }
}