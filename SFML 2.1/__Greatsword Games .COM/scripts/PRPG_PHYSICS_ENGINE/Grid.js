//a^2 + b^2 = c^2

//Create the initial grid
function create_grid(x, y){
	var grid = [];

	for (var i=0; i<x; i++){
		grid.push([]);
		for (var j=0; j<y; j++){
			grid[i].push("");
		}
	}

	return grid;
}

//Update the grid with current object positions
function update_grid(objects){

}

function getMousePosition(eventData){ //this is very specific to the levelEditor, need to make this more dynamic
	var mousePosition = {x:0, y:0};

	//there needs to be a way to seperate this from being levelEditor specific
	var gridScale = window.shell.gridScale;
	var scrollLeft = levelEditor.scrollLeft;
	var scrollTop = levelEditor.scrollTop;

	var shellElem =  $('shell');
	var screenElem = $(screenId);
	var windowPadding = (window.innerWidth - parseInt(shellElem.style.width))/2;
	var screenLeft = windowPadding >= 0 ? ( windowPadding + parseInt(shell.getStyle(screenElem,"left")) ) : parseInt(shell.getStyle(screenElem,"left"));
	var screenTop = parseInt(shell.getStyle(screenElem,"top"));
	//instead of using '100' see about using the grid/screen elements left and top values?
	//var gridLeft = (window.innerWidth - parseInt(shell.style.width))/2 + parseInt(screenElem.style.left); //100 is the toolBar

	mousePosition.x = event.x - screenLeft + scrollLeft;
	mousePosition.y = event.y - screenTop + scrollTop;
	return mousePosition;
}

/*
** Uses the event data from onClick to determine the x and y coordinates on the Grid.
** Returns the x and y values.
*/
function getGridPosition(eventData){ //this is very specific to the levelEditor, need to make this more dynamic
	var positionInfo = {x:0, y:0};

	//there needs to be a way to seperate this from being levelEditor specific
	var gridScale = window.shell.gridScale;
	var scrollLeft = levelEditor.scrollLeft;
	var scrollTop = levelEditor.scrollTop;

	var shellElem =  $('shell');
	var screenElem = $(screenId);
	var windowPadding = (window.innerWidth - parseInt(shellElem.style.width))/2;
	var screenLeft = windowPadding >= 0 ? ( windowPadding + parseInt(shell.getStyle(screenElem,"left")) ) : parseInt(shell.getStyle(screenElem,"left"));
	var screenTop = parseInt(shell.getStyle(screenElem,"top"));
	//instead of using '100' see about using the grid/screen elements left and top values?
	//var gridLeft = (window.innerWidth - parseInt(shell.style.width))/2 + parseInt(screenElem.style.left); //100 is the toolBar

	positionInfo.x = Math.floor((event.x - screenLeft + scrollLeft) / gridScale);
	positionInfo.y = Math.floor((event.y - screenTop + scrollTop) / gridScale);
	return positionInfo;
}

function getGridData(x, y, w, h){
	if(!w && !h)
		var gridData = levelEditor.grid[x][y];
	//gridData.push(levelEditor.grid[x][y]); //move grid to shell so it's in a common place for all applications of the editor or games
	//Add support for width and height

	return gridData;
}

/*function addObj(objInfo){
	//none_312px,192px,12px,12px (example: type_left,top,width,height)

	var gui = $('gui');
	var e_gui = $('e_gui');
	if(gui)
		var grid = this.game.grid;
	if(e_gui)
		var grid = window.levelEditor.grid;

	var gridScale = window.shell.gridScale;

	var id = objInfo;
	var objInfo = objInfo.split(",");

	var type = objInfo[0];
	var left = parseInt(objInfo[1])/gridScale;
	var top = parseInt(objInfo[2])/gridScale;
	var width = parseInt(objInfo[3])/gridScale;
	var height = parseInt(objInfo[4])/gridScale;

	for (var i=left; i<(left+width); i++){ //errors in cases like left of 4 and width of 1
		for (var j=top; j<(top+height); j++){ //errors in cases like top of 4 and height of 1
			grid[i][j] = id;
		}
	}
}*/

function updateObj(objInfo){
	var gui = $('gui');
	var e_gui = $('e_gui');
	if(gui)
		var grid = this.game.grid;
	if(e_gui)
		var grid = window.levelEditor.grid;

	var gridScale = window.shell.gridScale;

	var id = objInfo;
	var objInfo = objInfo.split(",");

	var type = objInfo[0];
	var left = parseInt(objInfo[1])/gridScale;
	var top = parseInt(objInfo[2])/gridScale;
	var width = parseInt(objInfo[3])/gridScale;
	var height = parseInt(objInfo[4])/gridScale;

	for (var i=left; i<(left+width); i++){
		for (var j=top; j<(top+height); j++){
			grid[i][j] = id;
		}
	}
}

function moveObj(objInfo){
	alert("success!!!");
}

function removeObj(objInfo){
	var gui = $('gui');
	var e_gui = $('e_gui');
	if(gui)
		var grid = this.game.grid;
	if(e_gui)
		var grid = window.levelEditor.grid;

	var gridScale = window.shell.gridScale;

	var id = objInfo; 
	var objInfo = objInfo.split(",");

	var type = objInfo[0];
	var left = parseInt(objInfo[1])/gridScale;
	var top = parseInt(objInfo[2])/gridScale;
	var width = parseInt(objInfo[3])/gridScale;
	var height = parseInt(objInfo[4])/gridScale;

	for (var i=left; i<(left+width); i++){
		for (var j=top; j<(top+height); j++){
			grid[i][j] = "";
		}
	}
}