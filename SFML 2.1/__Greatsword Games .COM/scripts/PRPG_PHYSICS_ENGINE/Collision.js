//Equation of a Line (point - slope form): y = m(x-Px) + Py
//Taking 2 line equations that are solved for y and x combine then
//ex. y = 3x-3
//	  y = 2.3x+4
//	  3x-3 = 2.3x+4
//Solve for x, then plug x into either line equation to find the y intersect
//You now have you (x,y) point of intersect for your 2 lines!

function update_current_position(objects) {
	for (var i in objects) {
		var object = objects[i];
		var current_position = object.current_position;
		var x = current_position.x,
			y = current_position.y,
			w = object.w,
			h = object.h;
		current_position.vertices.push({"x": x, "y": y});
		current_position.vertices.push({"x": x + w, "y": y});
		current_position.vertices.push({"x": x + w, "y": y + h});
		current_position.vertices.push({"x": x, "y": y + h});
		object.current_position = current_position;
	}
}

function update_pending_position(object) {
	var current_position = object.current_position,
		pending_position = object.pending_position;
	var x = current_position.x,
		y = current_position.y,
		w = object.w,
		h = object.h;
	pending_position.vertices = [];
	pending_position.vertices.push({"x": x, "y": y});
	pending_position.vertices.push({"x": x + w, "y": y});
	pending_position.vertices.push({"x": x + w, "y": y + h});
	pending_position.vertices.push({"x": x, "y": y + h});
	object.pending_position = pending_position;
}

function collision_check_group(group) {
	var detection = {};
	var collided;
	var collisions = 0,
		non_collisions = 0;

	//clear last frames detections
	for (var i in group){
		var A = group[i];
		A.collision_checked = {};
	}

	for (var i in group){
		var A = group[i];

		for (var j in group){
			var B = group[j];

			if (A != B && !A.collision_checked[B]) {
				collided = collision_check_overlap(A, B) ? true : false;

				A.collision_checked[B]=true;
				B.collision_checked[A]=true;
				if (collided) {
					collisions++;
				} else {
					non_collisions++;
				}
			}
		}
	}

	detection.collisions = collisions;
	detection.non_collisions = non_collisions;

	return detection;
}

function collision_check_overlap(A, B) {
	var X_collision_vertices = [],
		Y_collision_vertices = [];

	var A_vertices = A.current_position.vertices,
		A_minX,
		A_maxX,
		A_minY,
		A_maxY,
		B_vertices = B.current_position.vertices,
		B_minX,
		B_maxX,
		B_minY,
		B_maxY;

	//Now what genius...
	if (A.pending_position.vertices) {

	}

	//min and max coordinates stored as a vertex for later collision detection reference
	A_minX = A_maxX = A_vertices[0];
	A_minY = A_maxY = A_vertices[0];
	B_minX = B_maxX = B_vertices[0];
	B_minY = B_maxY = B_vertices[0];

    for(var i=0; i<A_vertices.length; i++) {
    	var vertex = A_vertices[i];

    	A_minX = (A_minX.x > vertex.x) ? vertex : A_minX;
    	A_maxX = (A_maxX.x < vertex.x) ? vertex : A_maxX;
    	A_minY = (A_minY.y > vertex.y) ? vertex : A_minY;
    	A_maxY = (A_maxY.y < vertex.y) ? vertex : A_maxY;
    }

    for(var i=0; i<B_vertices.length; i++) {
    	var vertex = B_vertices[i];

    	B_minX = (B_minX.x > vertex.x) ? vertex : B_minX;
    	B_maxX = (B_maxX.x < vertex.x) ? vertex : B_maxX;
    	B_minY = (B_minY.y > vertex.y) ? vertex : B_minY;
    	B_maxY = (B_maxY.y < vertex.y) ? vertex : B_maxY;
    }

    var vtc = [A_minX,A_maxX,A_minY,A_maxY,B_minX,B_maxX,B_minY,B_maxY]; //vertices to check

    //Check for collisions on the x-axis
    if ( isBetween(A_minX.x,B_minX.x,B_maxX.x) ) {
    	X_collision_vertices.push(A_minX);
    }
    if ( isBetween(A_maxX.x,B_minX.x,B_maxX.x) ) {
    	X_collision_vertices.push(A_maxX);
    }
    if ( isBetween(B_minX.x,A_minX.x,A_maxX.x) ) {
    	X_collision_vertices.push(B_minX);
    }
    if ( isBetween(B_maxX.x,A_minX.x,A_maxX.x) ) {
    	X_collision_vertices.push(B_maxX);
    }

    if (!X_collision_vertices.length) { //If no x-axis collisions we're done here
    	return false;
    }

    //Check for collisions on the y-xaxis
    if ( isBetween(A_minY.y,B_minY.y,B_maxY.y) ) {
    	Y_collision_vertices.push(A_minY);
    }
    if ( isBetween(A_maxY.y,B_minY.y,B_maxY.y) ) {
    	Y_collision_vertices.push(A_maxY);
    }
    if ( isBetween(B_minY.y,A_minY.y,A_maxY.y) ) {
    	Y_collision_vertices.push(B_minY);
    }
    if ( isBetween(B_maxY.y,A_minY.y,A_maxY.y) ) {
    	Y_collision_vertices.push(B_maxY);
    }

    if (!Y_collision_vertices.length) { //If no y-axis collisions we're done here
    	return false;
    }

    //We have collision on both the x-axis and y-axis, now we must use the collision vertices we collected to create line equations and 
    //check if the point of intercept happens within the objects or not

    return true; 
}











/*var collision = shell.collision = {};

collision.detectCollision = function(element, x, y, w, h){
	var results = {};
	var collided = results.collided = false;
	var objInfo = results.objInfo = [];

	if(x<0 || y<0 || w<0 || h<0)
		return results;

	var gui = $('gui');
	var e_gui = $('e_gui');
	if(gui)
		var grid = this.game.grid;
	if(e_gui)
		var grid = window.levelEditor.grid;

	//var gridScale = window.shell.gridScale;

	//var id = objInfo;
	//var objInfo = objInfo.split(",");

	//var type = objInfo[0];
	var left = parseInt(objInfo[1])/gridScale;
	var top = parseInt(objInfo[2])/gridScale;
	var width = parseInt(objInfo[3])/gridScale;
	var height = parseInt(objInfo[4])/gridScale;

	for (var i=x; i<(x+w); i++){
		for (var j=y; j<(y+h); j++){
			var currBlock = grid[i][j];
			 
			if(currBlock != element && currBlock != ""){
				var inArray = false;
				for (var k=0; k<objInfo.length; k++){
					if(objInfo[k] == currBlock){
						inArray = true;
					}
				}
				if (!inArray){
					results.collided = true;
					objInfo.push(currBlock);
				}
			}
		}
	}
	return results;
}*/

/*function checkBounds(key){
	var player = this.player;
	
	switch(key){
		case "W":
			var allow = (player.top <= 0) ? false : true;
			return allow;
			break;
		case "A":
			var allow = (player.left <= 0) ? false : true;
			return allow;
			break;
		case "S":
			var allow = (player.bottom >= 700) ? false : true;
			return allow;
			break;
		case "D":
			var allow = (player.right >= 1100) ? false : true;
			return allow;
			break;
		default:
			break;
	}
}*/

/*function buildMapBoundaries(){
	
	// Grabs the current MapContainer (for undefined purposes (7/31/2012)
	// Grabs the current world element and creates a mapBoundaries (x) and (y) grid using values 0 (empty) and 1 (filled?) to determine collision
	
	var MapContainer = $("MapContainer");
	var world = $("world"),
		worldWidth = parseInt(world.style.width),
		worldHeight = parseInt(world.style.height);
	
	
	var mapBoundaries = this.mapBoundaries;
	for (var i=0; i<worldWidth; i++){
		mapBoundaries["x"][i] = 0;
	}
	for (var i=0; i<worldHeight; i++){
		mapBoundaries["y"][i] = 0;
	}
	
	var worldObjects = world.childNodes;
	for (var i=0; i<worldObjects.length; i++){
		if (worldObjects[i].id == "object"){
			var object = worldObjects[i],
				objLeft = parseInt(object.style.left),
				objTop = parseInt(object.style.top),
				objWidth = parseInt(object.style.width),
				objHeight = parseInt(object.style.height),
				objRight = objLeft + (objWidth-1),
				objBottom = objTop + (objHeight-1);
			
			for (var j=objLeft; j<=objRight; j++){
				mapBoundaries["x"][j] = 1;
			}
			for (var j=objTop; j<=objBottom; j++){
				mapBoundaries["y"][j] = 1;
			}
		}
	}
	//grab each childNode of the "world" div
	//for each child grab their (left (x) + width (x2)) and (top (y) + height (y2)) and add all the values in those ranges...
	//to mapBoundaries(x) and mapBoundaries(y)
	debugger;
}*/