//F = M * A
//A = F / M
//D = A * T (distance / second)


function translate_object(object, direction, force, delta_time) {
	var x = direction.x || 0,
		y = direction.y || 0;

	//8 WAY movement
	var acceleration = force / (object.mass || 0);
	var distance = acceleration * delta_time;

	object.pending_position.x += x * distance; 
	object.pending_position.y += y * distance;
	update_pending_position(object);
}