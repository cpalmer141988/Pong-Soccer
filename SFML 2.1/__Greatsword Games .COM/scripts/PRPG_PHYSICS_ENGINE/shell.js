gridScale = shell.gridScale = 16 * scale;

shell.setScale = function(scale){
	var scale = top.scale = scale;
	shell.setResolution(scale)
}

shell.setResolution = function(scale){
	var shell = $('shell');

	var gui = $('gui');
	var e_gui = $('e_gui');

	var game = $('game');
	var e_grid = $('e_grid');
	var toolInterfaceContainer = $('toolInterfaceContainer');

	//100px are reserved for the border area of the shell
	if (shell){
		shell.style.width = 1280 * scale + "px";
		shell.style.height = 720 * scale + "px";
	}
	if (e_gui){
		e_gui.style.width = 1280 * scale - 100 + "px";
		e_gui.style.height = 720 * scale - 100 + "px";
	}
	if (e_grid){
		e_Grid.style.width = 1280 * scale - 100 + "px";
		e_Grid.style.height = 720 * scale - 100 + "px";
	}
	if (toolInterfaceContainer){
		toolInterfaceContainer.style.width = 1280 * scale - 100 + "px";
		toolInterfaceContainer.style.height = 720 * scale - 100 + "px";
	}

}

shell.showTooltip = function(event){

}

shell.getStyle = function(element, cssRule){
	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle){
		strValue = document.defaultView.getComputedStyle(element, "").getPropertyValue(cssRule);
	}
	else if(element.currentStyle){
		cssRule = cssRule.replace(/\-(\w)/g, function (strMatch, p1){
			return p1.toUpperCase();
		});
		strValue = element.currentStyle[cssRule];
	}
	return strValue;
}

function $(element){
	return document.getElementById(element);
}

//NEED TO REWORK THESE
shell.getWindowSize = function(){
	var size = [0, 0];
	if (typeof window.innerWidth != "undefined") {
		size = [window.innerWidth, window.innerHeight];
	}
	else if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientWidth != "undefined" && document.documentElement.clientWidth != 0) {
		size = [document.documentElement.clientWidth, document.documentElement.clientHeight];
	}
	else {
		size = [document.getElementsByTagName("body")[0].clientWidth, document.getElementsByTagName("body")[0].clientHeight];
	}
	return size;
}

shell.setSize = function(){
	var shell = $("shell");
	var header = $("header");
	var navigation = $("navigation");
	var account = $("account");
	var footer = $("footer");

	var size = top.shell.getWindowSize();
	var w = size[0];
	var h = size[1];
	/*var accountWidth = parseInt(account.style.width);
	var footerTop = h - parseInt(footer.style.height);*/
	
	/*shell.style.width = w < 950 ? "950px" : w+"px";
	shell.style.height = h < 550 ? "550px" : h+"px";
	header.style.width = w < 950 ? "950px" : (w-accountWidth)+"px";
	navigation.style.width = w < 950 ? "950px" : (w-accountWidth)+"px";
	account.style.left = (w-accountWidth)+"px";
	footer.style.width = w < 950 ? "950px" : w+"px";
	footer.style.top = footerTop+"px";*/

	shell.style.visibility = "visible";
}

shell.getXMLHttp = function (){
	var xmlhttp;
	if (window.XMLHttpRequest){			// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {							// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}