function display_menu(menu) {
	if (menu) {
		setDisplay(menu, true);
	}
	setDisplay(PRPG.menu_id, false);
	PRPG.menu_id = menu || null;
}

function display_game(state) {
	if (state) {
		setDisplay("background", true);
		setDisplay("foreground", true);
		setDisplay("hud", true);
	} else {
		setDisplay("background", false);
		setDisplay("foreground", false);
		setDisplay("hud", false);
	}
}