function inherit(superClass){
	var subClass = {};
	subClass.prototype = new superClass();
	return subClass.prototype;
}

//was this way when I first learned this long ago
//function inherit(superClass, subClass){
	//subClass.prototype = new superClass();
//}

function $(element){
	var element = (typeof element === "string") ? document.getElementById(element) : element;
	return element;
}

function ctx$(element, dimensions){
	var c = $(element);
	var ctx = c.getContext(dimensions || "2d");
	return ctx;
}

function px(num){
	return num+"px";
}

function isBetween(num, min, max, exclusive){
	var boolean = false;
	if (exclusive) {
		boolean = (num > min && num < max) ? true : false;
	}  else {
		boolean = (num >= min && num <= max) ? true : false;
	}
	return boolean;
}

function extract(array, index){
	if (index == 0){
		array.shift();
	} else if (index == array.length - 1){
		array.pop();
	} else {
		var first_half = array.slice(0,index);
		var second_half = array.slice(index+1,array.length);
		var new_array = first_half.push(second_half);
	}

	return new_array || array;
}

function setSize(element, dimensions){
	var element = $(element);
	element.style.width = px(dimensions[0]);
	element.style.height = px(dimensions[1]);
}

function setResolution(resolution){
	//should grab the body element and scale it to the resolution specified
}

function getCssRule(selectorText, path){
	var doc = path || document;
	var stylesheets = doc.styleSheets;

	for (var i=0; i<stylesheets.length; i++){
		var stylesheet = stylesheets[i];
		var rules = stylesheet.rules;
		if (!rules){
			rules = stylesheets.cssRules;
		}

		for (var j=0; j<rules.length; j++){
			var rule = rules[j];
			
			if (rule.selectorText == selectorText){
				return rule;
			}
		}
	}
}

//don't remember using this, so... we'll see..
function getStyle(element, cssRule){
	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle){
		strValue = document.defaultView.getComputedStyle(element, "").getPropertyValue(cssRule);
	}
	else if(element.currentStyle){
		cssRule = cssRule.replace(/\-(\w)/g, function (strMatch, p1){
			return p1.toUpperCase();
		});
		strValue = element.currentStyle[cssRule];
	}
	return strValue;
}

/*//convert to gridScale
function toGS(number){
	return number * top.gridScale;
}

//convert from gridScale
function fromGS(number){
	return number / top.gridScale;
}*/





function getWindowSize(){
	var size = [0, 0];
	if (typeof window.innerWidth != "undefined") {
		size = [window.innerWidth, window.innerHeight];
	}
	else if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientWidth != "undefined" && document.documentElement.clientWidth != 0) {
		size = [document.documentElement.clientWidth, document.documentElement.clientHeight];
	}
	else {
		size = [document.getElementsByTagName("body")[0].clientWidth, document.getElementsByTagName("body")[0].clientHeight];
	}
	return size;
};

function getAspectRatio(windowSize){
	//16:9 = 1.77
	//4:3 = 1.33
	var ratio = windowSize[0] / windowSize[1];
	if (ratio > 1.78){
		aspectRatio = "16:9";
	} else {
		aspectRatio = "4:3";
	}

	return aspectRatio;
}

function getScreenSize(windowSize, aspectRatio){
	var screenSize = [];
	var x = windowSize[0],
		y = windowSize[1];

	switch(aspectRatio){
		case "16:9":
			var h = y - 50;
			var w = h * 1.78;
			screenSize.push(w, h);
			break;
		case "4:3":
			var h = y - 50;
			var w = h * 1.33;
			screenSize.push(w, h);
			break;
	}

	return screenSize;
}






function fitToContainerElement(element){
	var element = $(element);
	//incomplete, obviously!
}

function stopBubbling(event){
	var e = event || window.event;
	e.cancelBubble = true;
	if (e.stopPropagation){
		e.stopPropagation();
	}
}

function isVisible(element){
	var state = false;
	if (typeof element === "string"){
		state = $(element).style.visibility == "visible" ? true : false;
	} else {
		state = element.style.visibility == "visible" ? true : false;
	}
	return state;
}

function setVisibility(element,state){
	var element = (typeof element === "string") ? $(element) : element;
	if (element){
		element.style.visibility = state ? "visible" : "hidden";
	}
}

function setDisplay(element,state){
	var element = (typeof element === "string") ? $(element) : element;
	if (element){
		element.style.display = state ? "block" : "none";
	}
}

function setBColor(element,color){
	var element = (typeof element === "string") ? $(element) : element;
	element.style.backgroundColor = color;
}

function getXMLHttp(){
	var xmlhttp;
	if (window.XMLHttpRequest){			// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {							// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}
