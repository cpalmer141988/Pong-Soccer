function set_canvas_resolution(element,w,h){
	var ctx = ctx$(element);
	var canvas = ctx.canvas;
	canvas.width = w;
	canvas.height = h;
}

function clear_canvas(element) {
	var ctx = ctx$(element);
	var canvas = ctx.canvas;
	ctx.clearRect(0,0,canvas.clientWidth,canvas.clientHeight);
}
















//Board


function render_board(element, board){
	var ctx = ctx$(element);

	var Screen = PRPG.Screen,
		screen_width = Screen.width,
		screen_height = Screen.height;

	var boardData = PRPG.Screen.get_boardSizeData();
	var slot_size = boardData.slot_size,
		left_edge = boardData.left_edge;

	var slot_fill_size = slot_size * 0.9,
		slot_fill_padding = slot_size * 0.05;

	var sideIDs = ["TOP", "BOTTOM"];

	//NEED TO IMPLEMENT THIS!!! - BUT IT DOESN'T WORK SO NEED TO KEEP THINKING OF OTHER WAYS TO IMPLEMENT SOMETHING LIKE THIS!!!
	if (Screen.resized){
		//remap board slot positions to the object reverse lookup map,
		//this will be used to determine what's being interacted with when clicks happen
		//this should be more accurate, and will allow the Renderer to tell the backend where
		//it put what, etc... (ex. "200,200,25,25" = "TOP,0,0" || x,y,w,h = "board["TOP"][x][y]")
	}

	for (var k=0; k<sideIDs.length; k++){
		var sideID = sideIDs[k];
		var side = board[sideID];

		for (var i=0; i<side.length; i++){

			for (var j=0; j<side[i].length; j++){
				var slot = side[i][j];

				switch(sideID){
					case "TOP":
						var y0 = slot_size * 6,
							x = left_edge+(slot_size*i)+slot_fill_padding,
							y = y0-(slot_size*j)+slot_fill_padding;

						ctx.strokeRect( left_edge+(slot_size*i) , y0-(slot_size*j) , slot_size , slot_size ); //draw outter boarder (grid)
						break;
					case "BOTTOM":
						var y0 =  slot_size * 8,
							x = left_edge+(slot_size*i)+slot_fill_padding,
							y = y0+(slot_size*j)+slot_fill_padding;

						ctx.strokeRect( left_edge+(slot_size*i) , y0+(slot_size*j) , slot_size , slot_size ); //draw outter boarder (grid)
						break;
				}

				ctx.strokeRect( left_edge+(slot_size*i) , y0+(slot_size*j) , slot_size , slot_size ); //draw outter boarder (grid)
			
				switch(slot.type){
					case "mana":
						ctx.fillStyle =  slot.color;
						ctx.fillRect( x , y , slot_fill_size , slot_fill_size);
						break;
					case "wall":
						ctx.fillStyle =  "brown";
						ctx.fillRect( x , y , slot_fill_size , slot_fill_size);

						ctx.font="24px Georgia";
						ctx.fillStyle = "black";
						ctx.fillText(slot.toughness, x + (slot_size * 0.35), y + (slot_size * 0.60));
						break;
					case "summoning_slot":
						ctx.fillStyle =  "yellow";
						ctx.fillRect( x , y , slot_fill_size , slot_fill_size);

						ctx.font="24px Georgia";
						ctx.fillStyle = "black";
						ctx.fillText(slot.multiplier, x + (slot_size * 0.35), y + (slot_size * 0.60));
						break;
					default:
						ctx.fillStyle =  "white";
						ctx.fillRect( x , y , slot_fill_size , slot_fill_size);
						break;
				}
			}
		}
	}
}



















//UI
function  render_player_guis(element, players){
	for (var i in players){
		var player = players[i];

		render_life_line(element,player);

		render_character(element, player);
		render_level(element, player);
		render_hp(element, player);
		render_mp(element, player);
		render_actions(element, player);
		render_mana(element, player);
		render_reinforcements(element, player);
	}
}

function render_character(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 3,
		h = slot_size * 5,
		image = $(player.portrait);

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (8.5 * slot_size),
				y = slot_size;
			ctx.strokeRect(x, y, w, h);
			ctx.drawImage(image, x, y, w, h);
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (3.5 * slot_size),
				y = (slot_size * 9);
			ctx.strokeRect(x, y, w, h);
			ctx.drawImage(image, x, y, w, h);
			break;
	}
}

function render_level(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size);

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (10.5 * slot_size),
				y = slot_size * 5.75;
			ctx.font="20px Georgia";
			ctx.fillStyle = "red";
			ctx.fillText("LV" + player.level, x, y);
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (3.25 * slot_size),
				y = (slot_size * 9.5);
			ctx.font="20px Georgia";
			ctx.fillStyle = "red";
			ctx.fillText("LV" + player.level, x, y);
			break;
	}
}

function render_hp(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 3,
		h = slot_size * 0.25;

	var percent_HP =  player.HP / player.maxHP,
		HP_bar_w = w * percent_HP;

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (8.5 * slot_size),
				y = slot_size * 0.75;
			ctx.fillStyle = "green";
			ctx.fillRect(x, y, HP_bar_w, h);
			ctx.strokeRect(x, y, w, h);
			ctx.font="16px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText("HP", x + (slot_size * 3.1), y + (slot_size * 0.24));
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (3.5 * slot_size),
				y = (slot_size * 14);
			ctx.fillStyle = "green";
			ctx.fillRect(x, y, HP_bar_w, h);
			ctx.strokeRect(x, y, w, h);
			ctx.font="16px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText("HP", x - (slot_size * 0.5), y + (slot_size * 0.23));
			break;
	}
}

function render_mp(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 3,
		h = slot_size * 0.25;

	var percent_MP =  player.MP / player.maxMP,
		MP_bar_w = w * percent_MP;

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (8.5 * slot_size),
				y = slot_size * 0.5;
			ctx.fillStyle = "blue";
			ctx.fillRect(x, y, MP_bar_w, h);
			ctx.strokeRect(x, y, w, h);
			ctx.font="16px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText("MP", x + (slot_size * 3.1), y + (slot_size * 0.23));
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (3.5 * slot_size),
				y = (slot_size * 14.25);
			ctx.fillStyle = "blue";
			ctx.fillRect(x, y, MP_bar_w, h);
			ctx.strokeRect(x, y, w, h);
			ctx.font="16px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText("MP", x - (slot_size * 0.5), y + (slot_size * 0.24));
			break;
	}
}

function render_actions(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 1,
		h = slot_size * 1;

	var actions = player.remaining_actions;

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (2.75 * slot_size),
				y = slot_size * 0.75;
			ctx.strokeRect(x, y, w, h);
			ctx.font="24px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText(actions, x + (slot_size * 0.35), y + (slot_size * 0.60));
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (9.75 * slot_size),
				y = (slot_size * 13.25);
			ctx.strokeRect(x, y, w, h);
			ctx.font="24px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText(actions, x + (slot_size * 0.35), y + (slot_size * 0.60));
			break;
	}
}

function render_mana(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 1,
		h = slot_size * 1;

	var reinforcements = player.max_mana - player.mana - player.summoning_slots;

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (1.5 * slot_size),
				y = slot_size * 0.75;
			ctx.strokeRect(x, y, w, h);
			ctx.font="24px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText(reinforcements, x + (slot_size * 0.35), y + (slot_size * 0.60));
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) + (8.5 * slot_size),
				y = (slot_size * 13.25);
			ctx.strokeRect(x, y, w, h);
			ctx.font="24px Georgia";
			ctx.fillStyle = "black";
			ctx.fillText(reinforcements, x + (slot_size * 0.35), y + (slot_size * 0.60));
			break;
	}
}

function render_reinforcements(element, player){
	var reinforcements = player.reinforcements;
}

function render_life_line(element, player){
	var ctx = ctx$(element);
	var placement = player.placement;

	var screen_width = PRPG.Screen.width,
		screen_height = PRPG.Screen.height;

	var slot_size = screen_height / 15,
		column_count =  Math.round(screen_width / slot_size),
		w = slot_size * 9,
		h = slot_size * 0.1;

	switch(placement){ 
		case "TOP":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (0.5 * slot_size),
				y = slot_size * 0.75;
			ctx.fillStyle = "green";
			ctx.fillRect(x, y, w, h);
			break;
		case "BOTTOM":
			var x = ( ( (column_count - 8) * slot_size) / 2) - (0.5 * slot_size),
				y = (slot_size * 14.15);
			ctx.fillStyle = "green";
			ctx.fillRect(x, y, w, h);
			break;
	}
}

function render_fps(element, frames){
	var ctx = ctx$(element);
	var x = PRPG.Screen.width * 0.95;
	var y = PRPG.Screen.height * 0.05;

	ctx.font="20px Georgia";
	ctx.fillStyle = "black";
	ctx.fillText(frames, x, y);
}

















